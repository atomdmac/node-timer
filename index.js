const MS_PER_HOUR = 3600000;

const getTimeDiff = (date1, date2) => date1 - date2;

const createTimer = (start) =>
  () => getTimeDiff(new Date(), start);

const createCountdown = (max) => {
  const timer = createTimer(new Date());
  return {
    timer,
    remainder: () => max - timer(),
    complete: () => timer() >= max
  }
};

const timerComplete = (max, timer) =>
  () => timer() >= max;

const safeParseInt = (intLike, defaultValue) =>
  parseInt(intLike, 10) || defaultValue || 1;

// const getRemainingMinutes = (ms) => (ms % MS_PER_HOUR) / 60000;
const getSecondsRemainder = (ms) => Math.round((ms % MS_PER_HOUR) % 60000 / 1000);

// Runnit!
const runnit = () => {
  const max = safeParseInt(process.argv[2]);
  const countdown = createCountdown(max * 1000);

  console.log('Waiting', max, 'seconds')

  const tick = () => {
    console.log('Countdown: ', getSecondsRemainder(countdown.remainder()));
    if (countdown.complete()) {
      console.log('Done!');
      clearInterval(interval);
    }
  }

  const interval = setInterval(tick, 1000);
}

runnit();
